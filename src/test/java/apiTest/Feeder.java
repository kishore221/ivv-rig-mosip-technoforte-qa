package test.java.apiTest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import main.java.io.mosip.ivv.base.BaseHelper;
import main.java.io.mosip.ivv.helpers.Controller;
import main.java.io.mosip.ivv.orchestrator.Scenario;
import main.java.io.mosip.ivv.utils.ParserEngine;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;
import java.lang.reflect.Method;
import java.util.ArrayList;
import main.java.io.mosip.ivv.utils.*;

public class Feeder {

    private static ExtentHtmlReporter htmlReporter;
    private static ExtentReports extent;
    public ExtentTest extentTest;
    private Scenario scenario;

    @BeforeSuite
    public void beforeSuite(){
    	System.out.println("inside before suite");
        htmlReporter = new ExtentHtmlReporter(BaseHelper.extentReportFile);
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);

        // Initialize audit logger
        Utils.setupLogger();
    }

    @AfterSuite
    public void afterSuite(){
        extent.flush();
    }

    @DataProvider(name="ScenarioDataProvider")
    public static Object[][] dataProvider(ITestContext context) {
    	System.out.println("inside data provider");
        String scenarioFilter = context.getCurrentXmlTest().getParameter("scenarioFilter");
        boolean stopOnError = Boolean.parseBoolean(context.getCurrentXmlTest().getParameter("stopOnError"));
        ParserEngine parser = new ParserEngine();
        ArrayList<Scenario> scenariosToRun = parser.fetch();
        Object[][] dataArray = new Object[scenariosToRun.size()][2];
        for(int i=0; i<scenariosToRun.size();i++){
            dataArray[i][0] = i;
            dataArray[i][1] = scenariosToRun.get(i);
        }
        return dataArray;
    }

    @BeforeMethod
    public void beforeMethod(Method method) {
        //System.out.println("Scenario name: " + method.getName());
    }

    @Test(dataProvider="ScenarioDataProvider")
    private void run(int i, Scenario s) {
    	//System.out.println("hello test");
        scenario = s;
        //Update extent report
        extentTest = extent.createTest("Scenario_" + scenario.name + ": " + scenario.description);
        //update audit log
        Utils.auditLog.info("");
        Utils.auditLog.info("-----------------------------------------------------------------------------------------------------------------");
        Utils.auditLog.info(">>> TEST Scenario: " + scenario.name + "."+ scenario.description);
        Utils.auditLog.info("-----------------------------------------------------------------------------------------------------------------");

        Controller ctrl = new Controller(scenario.data, extentTest);
        for(Scenario.Step step: scenario.steps) {
            //System.out.println(scenario.name+"-"+ step.name);
            extentTest.info("Test Step: " + step.name);
            Utils.auditLog.info("Test Step: " + step.name);
            ctrl.run(step);
        }
       // System.out.println("G E T - Call Records: " + ctrl.getCallRecords());
        //Utils.auditLog.info(ctrl.getCallRecords().toString());
    }

    @AfterMethod
    public void afterMethod(ITestResult result) {
        switch(result.getStatus()){
            case ITestResult.FAILURE:
                extentTest.fail("Scenario failed");
                break;

            case ITestResult.SKIP:
                extentTest.fail("Scenario skipped");
                break;

            case ITestResult.SUCCESS:

                break;
        }
    }
}
