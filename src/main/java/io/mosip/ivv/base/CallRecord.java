package main.java.io.mosip.ivv.base;

import com.aventstack.extentreports.Status;
import io.restassured.response.Response;
import main.java.io.mosip.ivv.utils.Utils;

public class CallRecord {
    public String method = "";
    public String url = "";
    public String input_data = "";
    public Response response = null;
    public String status = "";
    public int statusCode;

    public CallRecord(String url, String method, String input_data, Response response, String status) {
        this.url = url;
        this.method = method;
        this.input_data = input_data;
        this.response = response;
        this.status = status;
        this.statusCode = response.getStatusCode();

        Utils.auditLog.info("# [method][url]: " + "["+method+"]["+url+"]");
        Utils.auditLog.info("# INPUT Data: " + input_data);
        Utils.auditLog.info("# RETURN Response: " + response.asString().toString());
        Utils.auditLog.info("STATUS Code: " + status);
        if (Integer.parseInt(status) == 200 && Integer.parseInt(status) < 300 ) {
            BaseHelper.extentTest.log(Status.PASS, "Method[" + method + "] executed with the status " + response.getStatusCode() + "(" + Utils.assertResponseStatus(status) + ")");
            Utils.auditLog.fine("HTTP Status: " + Utils.assertResponseStatus(status));
        } else if (Integer.parseInt(status) >= 300 && Integer.parseInt(status) < 400 ) {
            BaseHelper.extentTest.log(Status.WARNING, "Method[" + method + "] executed with the status " + response.getStatusCode() + "(" + Utils.assertResponseStatus(status) + ");");
            Utils.auditLog.warning( "HTTP Status: " + Utils.assertResponseStatus(status));
        } else if (Integer.parseInt(status) >= 400 && Integer.parseInt(status) < 599 ) {
            BaseHelper.extentTest.log(Status.ERROR, "Method[" + method + "] executed with the status " + response.getStatusCode() + "(" + Utils.assertResponseStatus(status) + ")");
            Utils.auditLog.severe("HTTP Status: " + Utils.assertResponseStatus(status));
        }
        Utils.auditLog.info("-----------------------------------------------------------------------------------------------------------------");
    }

    public CallRecord(){

    }

    public String getMethod(){
        return method;
    }

    public String getInputData(){
        return input_data;
    }

    public Response getResponse(){
        return response;
    }

    public String getStatus(){
        return status;
    }

    public int getStatusCode() {
            return statusCode;
    }
}
