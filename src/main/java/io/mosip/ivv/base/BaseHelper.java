package main.java.io.mosip.ivv.base;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import main.java.io.mosip.ivv.orchestrator.Scenario;
import main.java.io.mosip.ivv.utils.Utils;
import org.testng.Assert;
import java.util.ArrayList;

public abstract class BaseHelper {

    public int assertionPolicy = 0;
    public int variation = 0;
    public static String baseUri = "https://mosiptf.southeastasia.cloudapp.azure.com";
    //public static String baseUri = "https://integ.mosip.io";
    public static String baseVersion = "v1.0";
    protected static ExtentTest extentTest;
    public static String extentReportFile = System.getProperty("user.dir") + "/testRun/reports/" + "mosip_reports.html";
    public static String auditLogFile = System.getProperty("user.dir") + "/testRun/logs/" + "mosip_auditLogs.log";

    public enum login_variations {
        EMAIL, PHONE, POB, POI, POA;
    }

    public enum assertion_policy {
        DONT, DEFAULT, STATUS, API_CALL, AUDIT_LOG, DB_VERIFICATION, COMMUNICATION_SINK, NotSupported, COMM_SINK, BLOCKED, ALL
    }

    public abstract void SetData(String json);
    public static ArrayList<CallRecord> calls = new ArrayList<CallRecord>();

    public static void AddCallRecord(CallRecord record){
        calls.add(record);
    }

    public static ReadContext stepErrorMiddleware(Scenario.Step st, CallRecord cr){
        Boolean api_body_status = false;
        String api_body_status_code = "";

//        /* API call assert */
//        Assert.assertEquals(cr.response.getStatusCode(), 200, "API self assert status code");
//
//        /* Response to map */
        ReadContext ctx = JsonPath.parse(cr.response.getBody().asString());
        api_body_status = (boolean) ctx.read("$['status']");
//
//        //status can be "true" or "false" or httpStatusCode
//        try {
//            if (!isNumeric(Boolean.toString(ctx.read("$['status']"))))
//                api_body_status = (boolean) ctx.read("$['status']");
//        } catch (ClassCastException e) {
//            if (!isNumeric(Integer.toString(ctx.read("$['status']"))))
//                api_body_status_code = Integer.toString(ctx.read("$['status']"));
//        }
//
//        if ((!api_body_status) && (!api_body_status_code.isEmpty())) {
//                extentTest.log(Status.FAIL, "API body status return as " + api_body_status.toString());
//                Utils.auditLog.severe("API body status return as " + api_body_status.toString());
//        }

        /* Error expectation/ success->true */
        if(api_body_status.compareTo(true) == 0){
            extentTest.log(Status.FAIL, "API body status Expected[false], Actual ["+api_body_status+"]");
        }
        Assert.assertEquals(api_body_status, (Boolean)false, "API body status");
        if(!ctx.read("$['err']['errorCode']").equals(st.error)) {
            extentTest.log(Status.FAIL, "Error code expected " + st.error + " but found " + ctx.read("$['err']['errorCode']"));
            Utils.auditLog.severe("Error code expected " + st.error + " but found " + ctx.read("$['err']['errorCode']"));
        }
        Assert.assertEquals(ctx.read("$['err']['errorCode']"), st.error, "error code");

//        Boolean api_body_status = (Boolean) ctx.read("$['status']");
//        /* Error expectation/ success->true */
//        if(!st.error.isEmpty()){
//            if(api_body_status) {
//                extentTest.log(Status.FAIL, "API body status return as " + api_body_status.toString());
//                Utils.auditLog.severe("API body status return as " + api_body_status.toString());
//            }
//            Assert.assertEquals(api_body_status, (Boolean)false, "API body status");
//            if(ctx.read("$['err']['errorCode']") != st.error) {
//                extentTest.log(Status.FAIL, "Error code expected " + st.error + " but found " + ctx.read("$['err']['errorCode']"));
//                Utils.auditLog.severe("Error code expected " + st.error + " but found " + ctx.read("$['err']['errorCode']"));
//            }
//            Assert.assertEquals(ctx.read("$['err']['errorCode']"), st.error, "error code");
//        }else{
//            if(!api_body_status) {
//                extentTest.log(Status.FAIL, "API body status return as " + api_body_status.toString());
//                Utils.auditLog.severe("API body status return as " + api_body_status.toString());
//            }
//            Assert.assertEquals(api_body_status, (Boolean)true, "API body status");
//        }

        return ctx;
    }

    public static boolean isNumeric(String inputStr) {
        try {
            double d = Double.parseDouble(inputStr);
        } catch (NumberFormatException | NullPointerException npe) {
                return false;
        }
        return true;
    }
}
