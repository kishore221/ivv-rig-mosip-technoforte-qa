package main.java.io.mosip.ivv.helpers;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import main.java.io.mosip.ivv.base.BaseHelper;
import main.java.io.mosip.ivv.base.CallRecord;
import main.java.io.mosip.ivv.base.CoreStructures;
import main.java.io.mosip.ivv.base.Persona;
import main.java.io.mosip.ivv.orchestrator.Scenario;
import main.java.io.mosip.ivv.utils.ResponseParser;
import main.java.io.mosip.ivv.utils.Utils;
import main.java.io.mosip.ivv.utils.UtilsA;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;

import java.util.HashMap;

import static io.restassured.RestAssured.given;

public class BookingService extends Controller{

    public BookingService(Scenario.Data dataSet, ExtentTest ex) {
        super(dataSet, ex);
    }

    public static CallRecord bookAppointment(Scenario.Step step){
        int index = UtilsA.getPersonIndex(step);

        /* getting slot */
        Scenario.Step nstep = new Scenario.Step();
        nstep.name = "getAvailableSlots";
        nstep.index.add(index);
        CallRecord slotObject = getAvailableSlots(nstep);
        CoreStructures.Slot slot = new ResponseParser().slot(slotObject.response.getBody().asString());
        data.persons.get(index).slot = slot;
        if(!slot.available){
            extentTest.log(Status.FAIL, "Appointment slots are not available!");
            Assert.assertEquals(false, true, "Appointment slot found");
        }
        /* getting slot */
        Persona person = data.persons.get(index);
        JSONObject api_input = new JSONObject();
        api_input.put("id", "mosip.pre-registration.booking.book");
        api_input.put("ver", "1.0");
        api_input.put("reqTime", Utils.getCurrentDateAndTimeForAPI());
        api_input.put("request", new JSONArray(){{
            add(new JSONObject(){{
                put("preRegistrationId", person.pre_registration_id);
                put("newBookingDetails", new JSONObject(){{
                    put("registration_center_id", person.registration_center_id);
                    put("appointment_date", slot.date);
                    put("time_slot_from", slot.from);
                    put("time_slot_to", slot.to);
                }});
            }});
        }});

        switch(step.variant){

            case "DEFAULT":
                break;

            default:
                extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
                Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
                return new CallRecord();

        }

        String url = "/pre-registration/" + baseVersion + "/booking/appointment";
        RestAssured.baseURI = baseUri;
        RestAssured.useRelaxedHTTPSValidation();
        Response api_response = (Response) given().contentType(ContentType.JSON).body(api_input).post(url);

        CallRecord res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;

                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        case API_CALL:
                            Scenario.Step ac_step = new Scenario.Step();
                            ac_step.name = "getAppointment";
                            ac_step.index.add(index);
                            CallRecord getAppointment = getAppointment(ac_step);
                            HashMap<String, Object> hm = ResponseParser.bookAppointmentResponseParser(data.persons.get(index), getAppointment.response);

                            extentTest.log((Status) hm.get("status"), hm.get("msg").toString());
                            if (hm.get("status").equals(Status.FAIL)) {
                                Utils.auditLog.severe(hm.get("msg").toString());
                            } else {
                                Utils.auditLog.info(hm.get("msg").toString());
                            }
                            Utils.auditLog.info("-----------------------------------------------------------------------------------------------------------------");

                            Assert.assertEquals(hm.get("status"), Status.INFO, hm.get("msg").toString());
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }

        return res;
    }

    public static CallRecord bookAppointmentAll(Scenario.Step step){
        switch (step.variant) {

            case "DEFAULT":
                break;

            default:
                extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
                Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
                return new CallRecord();
        }
        CallRecord res = null;

        for (int i = 0; i < data.persons.size(); i++) {
            /* getting slot */
            Scenario.Step nstep = new Scenario.Step();
            nstep.name = "getAvailableSlots";
            nstep.variant = "DEFAULT";
            nstep.index.add(i);
            CallRecord slotObject = getAvailableSlots(nstep);
            CoreStructures.Slot slot = new ResponseParser().slot(slotObject.response.getBody().asString());
            data.persons.get(i).slot = slot;
            if(!slot.available){
                extentTest.log(Status.FAIL, "Appointment slots are not available!");
                Assert.assertEquals(false, true, "Appointment slot");
            }
            /* getting slot */

            JSONObject api_input = new JSONObject();
            api_input.put("id", "mosip.pre-registration.booking.book");
            api_input.put("ver", "1.0");
            api_input.put("reqTime", Utils.getCurrentDateAndTimeForAPI());
            int finalI = i;
            api_input.put("request", new JSONArray(){{
                add(new JSONObject(){{
                    put("preRegistrationId", data.persons.get(finalI).pre_registration_id);
                    put("newBookingDetails", new JSONObject(){{
                        put("registration_center_id", data.persons.get(finalI).registration_center_id);
                        put("appointment_date", slot.date);
                        put("time_slot_from", slot.from);
                        put("time_slot_to", slot.to);
                    }});
                }});
            }});

            String url = "/pre-registration/" + baseVersion + "/booking/appointment";
            RestAssured.baseURI = baseUri;
            RestAssured.useRelaxedHTTPSValidation();
            Response api_response = (Response) given().contentType(ContentType.JSON).body(api_input).post(url);

            res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
            AddCallRecord(res);

            /* check for api status */
            if(api_response.getStatusCode() != 200){
                extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
                Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
                Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
            }

            /* Error handling middleware */
            if(step.error != null && !step.error.isEmpty()){
                stepErrorMiddleware(step, res);
            }else{
                ReadContext ctx = JsonPath.parse(api_response.getBody().asString());
                /* Response data parsing */

                /* Assertion policies execution */
                if(step.asserts.size()>0){
                    for (assertion_policy assertion_type : step.asserts) {
                        switch (assertion_type) {
                            case DONT:
                                break;

                            case STATUS:
                                if(!(Boolean) ctx.read("$['status']")){
                                    extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                    Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                    Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                                }
                                break;

                            case DEFAULT:
                                if(!(Boolean) ctx.read("$['status']")){
                                    extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                    Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                    Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                                }
                                break;

                            default:
                                extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                                Utils.auditLog.warning("Skipping assert "+assertion_type);
                                break;
                        }
                    }
                }
            }

        }
        return res;
    }

    public static CallRecord reBookAppointment(Scenario.Step step){
        switch (step.variant) {

            case "DEFAULT":
                break;

            default:
                extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
                Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
                return new CallRecord();
        }
        int index = UtilsA.getPersonIndex(step);
        Persona person = data.persons.get(index);

        Scenario.Step nstep = new Scenario.Step();
        nstep.name = "getAvailableSlot";
        nstep.variant = "DEFAULT";
        nstep.index.add(index);
        CallRecord slotObject = getAvailableSlots(nstep);
        CoreStructures.Slot slot = new ResponseParser().slot(slotObject.response.getBody().asString());
        if(!slot.available){
            Assert.assertEquals(false, true, "Appointment slot");
        }
        JSONObject api_input = new JSONObject();
        api_input.put("id", "mosip.pre-registration.booking.book");
        api_input.put("ver", "1.0");
        api_input.put("reqTime", Utils.getCurrentDateAndTimeForAPI());
        api_input.put("request", new JSONArray(){{
            add(new JSONObject(){{
                put("preRegistrationId", person.pre_registration_id);
                put("oldBookingDetails", new JSONObject(){{
                    put("registration_center_id", person.registration_center_id);
                    put("appointment_date", person.slot.date);
                    put("time_slot_from", person.slot.from);
                    put("time_slot_to", person.slot.to);
                }});
                put("newBookingDetails", new JSONObject(){{
                    put("registration_center_id", person.registration_center_id);
                    put("appointment_date", slot.date);
                    put("time_slot_from", slot.from);
                    put("time_slot_to", slot.to);
                }});
            }});
        }});

        String url = "/pre-registration/" + baseVersion + "/booking/appointment";
        RestAssured.baseURI = baseUri;
        RestAssured.useRelaxedHTTPSValidation();
        Response api_response = (Response) given().contentType(ContentType.JSON).body(api_input).post(url);

        CallRecord res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;

                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }

        return res;
    }

    public static CallRecord getAvailableSlots(Scenario.Step step){
        switch (step.variant) {

            case "DEFAULT":
                break;

            default:
                extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
                Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
                return new CallRecord();
        }
        int index = UtilsA.getPersonIndex(step);
        Persona person = data.persons.get(index);

        JSONObject api_input = new JSONObject();
        api_input.put("registration_center_id", person.registration_center_id);
        String url = "/pre-registration/" + baseVersion + "/booking/appointment/availability";
        RestAssured.baseURI = baseUri;
        RestAssured.useRelaxedHTTPSValidation();
        Response api_response = (Response) given().queryParam("registration_center_id", person.registration_center_id)
                .get(url);
        CallRecord res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;

                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }

        return res;
    }

    public static CallRecord getAppointment(Scenario.Step step){
        switch (step.variant) {

            case "DEFAULT":
                break;

            default:
                extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
                Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
                return new CallRecord();
        }
        int index = UtilsA.getPersonIndex(step);
        Persona person = data.persons.get(index);

        JSONObject api_input = new JSONObject();
        api_input.put("pre_registration_id", person.pre_registration_id);
        String url = "/pre-registration/" + baseVersion + "/booking/appointment";
        RestAssured.baseURI = baseUri;
        RestAssured.useRelaxedHTTPSValidation();
        Response api_response = (Response) given()
                .queryParam("pre_registration_id", person.pre_registration_id)
                .get(url);
        CallRecord res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;

                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }

        return res;
    }

    public static CallRecord cancelAppointment(Scenario.Step step){
        switch (step.variant) {

            case "DEFAULT":
                break;

            default:
                extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
                Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
                return new CallRecord();
        }
        int index = UtilsA.getPersonIndex(step);
        Persona person = data.persons.get(index);
        JSONObject api_input = new JSONObject();
        api_input.put("id", "mosip.pre-registration.booking.book");
        api_input.put("ver", "1.0");
        api_input.put("reqTime", Utils.getCurrentDateAndTimeForAPI());
        api_input.put("request", new JSONObject(){{
            put("pre_registration_id", person.pre_registration_id);
            put("registration_center_id", person.registration_center_id);
            put("appointment_date", person.slot.date);
            put("time_slot_from", person.slot.from);
            put("time_slot_to", person.slot.to);
        }});

        String url = "/pre-registration/" + baseVersion + "/booking/appointment";
        RestAssured.baseURI = baseUri;
        RestAssured.useRelaxedHTTPSValidation();
        Response api_response = (Response) given().contentType(ContentType.JSON).body(api_input)
                .put(url);

        CallRecord res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;

                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }

        return res;
    }

}
