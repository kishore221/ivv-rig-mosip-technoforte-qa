/*
 *  Author: Channakeshava
 */
package main.java.io.mosip.ivv.helpers;
import java.io.File;
import java.util.HashMap;

import com.aventstack.extentreports.Status;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.jayway.jsonpath.ReadContext;
import main.java.io.mosip.ivv.base.Persona;
import main.java.io.mosip.ivv.base.ProofDocument;
import main.java.io.mosip.ivv.orchestrator.Scenario;
import org.json.simple.JSONObject;
import main.java.io.mosip.ivv.utils.*;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import main.java.io.mosip.ivv.base.CallRecord;
import org.testng.Assert;

public class DocumentService extends Controller {
    public DocumentService(Scenario.Data data) {
        super(data, extentTest);
    }

    public static CallRecord addDocCategory(Scenario.Step step) {
        // data: AddDocCategory(POB1)
        return new CallRecord();
    }

    public static CallRecord addDocFormat(Scenario.Step step) {
        // data: AddDocFormat(JPG)
        return new CallRecord();
    }

    @SuppressWarnings("unchecked")
    public static CallRecord addDocument(Scenario.Step step) {
        CallRecord res = null;
        int index = UtilsA.getPersonIndex(step);
        Persona person = data.persons.get(index);

        for (int j=0; j < person.documents.size(); j++) {
            ProofDocument proofDocument = data.persons.get(index).documents.get(j);
            JSONObject request_json = new JSONObject();
            JSONObject api_input = new JSONObject();
            request_json.put("pre_registartion_id", person.pre_registration_id);
            request_json.put("doc_cat_code", proofDocument.doc_cat_code.toString());
            request_json.put("doc_typ_code", proofDocument.doc_type_code);
            request_json.put("lang_code", person.lang_code);
            request_json.put("doc_file_format", proofDocument.doc_file_format);
            request_json.put("status_code", person.pre_registration_status_code);
            request_json.put("upload_by", person.phone);
            request_json.put("upload_date_time", Utils.getCurrentDateAndTimeForAPI());

            switch (step.variant) {
                case "invalidFormatCategory":
                    request_json.put("doc_cat_code", data.globals.get("INVALID_DOCCATEGORY"));
                    break;

                case "InvalidFormat":
                    request_json.put("doc_file_format", data.globals.get("INVALID_DOCTYPE"));
                    break;

                case "InvalidPRID":
                    request_json.put("pre_registartion_id", data.globals.get("INVALID_PRID"));
                    break;

                case "DEFAULT":
                    break;

                default:
                    extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
                    Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
                    return new CallRecord();
            }

            api_input.put("id", prop.getProperty("documentUploadID"));
            api_input.put("ver", prop.getProperty("apiver"));
            api_input.put("reqTime", Utils.getCurrentDateAndTimeForAPI());
            api_input.put("request", request_json);

            // submit /documents POST service
            String url = "/pre-registration/" + baseVersion + "/document/documents";
            RestAssured.baseURI = baseUri;
            Response api_response = RestAssured
                    .given().relaxedHTTPSValidation()
                    .multiPart("file", new File(proofDocument.path))
                    .formParam("Document request", api_input)
                    .contentType("multipart/form-data")
                    .post(url);

            res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
            AddCallRecord(res);

            /* check for api status */
            if(api_response.getStatusCode() != 200){
                extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
                Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
                Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
            }

            /* Error handling middleware */
            if(step.error != null && !step.error.isEmpty()){
                ReadContext ctx = stepErrorMiddleware(step, res);
            }else{
                ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(api_response.getBody().asString());
                /* Response data parsing */
                try {
                    data.persons.get(index).documents.get(j).doc_id = ctx.read("$['response'][0]['documentId']");
                } catch (PathNotFoundException e){

                }

                /* Assertion policies execution */
                if(step.asserts.size()>0){
                    for (assertion_policy assertion_type : step.asserts) {
                        switch (assertion_type) {
                            case DONT:
                                break;

                            case API_CALL:
                                Scenario.Step nstep = new Scenario.Step();
                                nstep.name = "getDocument";
                                nstep.index.add(index);
                                try {
                                    Thread.sleep(3000);
                                } catch (InterruptedException e) {

                                }

                                CallRecord getAppRecord = getDocuments(nstep);

                                HashMap<String, Object> hm = ResponseParser.addDocumentResponseParser(data.persons.get(index).documents.get(j), getAppRecord.response);

                                extentTest.log((Status) hm.get("status"), hm.get("msg").toString());
                                if (hm.get("status").equals(Status.FAIL)) {
                                    Utils.auditLog.severe(hm.get("msg").toString());
                                } else {
                                    Utils.auditLog.info(hm.get("msg").toString());
                                }
                                Utils.auditLog.info("-----------------------------------------------------------------------------------------------------------------");

                                Assert.assertEquals(hm.get("status"), Status.INFO, hm.get("msg").toString());
                                break;

                            case STATUS:
                                if(!(Boolean) ctx.read("$['status']")){
                                    extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                    Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                    Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                                }
                                break;

                            case DEFAULT:
                                if(!(Boolean) ctx.read("$['status']")){
                                    extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                    Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                    Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                                }
                                break;

                            default:
                                extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                                Utils.auditLog.warning("Skipping assert "+assertion_type);
                                break;
                        }
                    }
                }
            }
        }

        return res;
    }

    @SuppressWarnings("unchecked")
    public static CallRecord addDocumentAll(Scenario.Step step) {
        CallRecord res = null;
        //TODO - Iteration for each persons
        for (int i = 0; i < data.persons.size(); i++) {

            Persona person = data.persons.get(i);
            //TODO - Iteration for each document with-in a person
            for (int j = 0; j < data.persons.get(i).documents.size(); j++) {
                ProofDocument proofDocument = data.persons.get(i).documents.get(j);
                JSONObject request_json = new JSONObject();
                JSONObject api_input = new JSONObject();
                request_json.put("pre_registartion_id", person.pre_registration_id);
                request_json.put("doc_cat_code", proofDocument.doc_cat_code.toString());
                request_json.put("doc_typ_code", proofDocument.doc_type_code);
                request_json.put("lang_code", person.lang_code);
                request_json.put("doc_file_format", proofDocument.doc_file_format);
                request_json.put("status_code", person.pre_registration_status_code);
                request_json.put("upload_by", person.phone);
                request_json.put("upload_date_time", Utils.getCurrentDateAndTimeForAPI());

                switch (step.variant) {
                    case "invalidFormatCategory":
                        request_json.put("doc_cat_code", data.globals.get("INVALID_DOCCATEGORY"));
                        break;

                    case "InvalidFormat":
                        request_json.put("doc_file_format", data.globals.get("INVALID_DOCTYPE"));
                        break;

                    case "InvalidPRID":
                        request_json.put("pre_registartion_id", data.globals.get("INVALID_PRID"));
                        break;

                    case "DEFAULT":
                        break;

                    default:
                        extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
                        Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
                        return new CallRecord();
                }

                api_input.put("id", prop.getProperty("documentUploadID"));
                api_input.put("ver", prop.getProperty("apiver"));
                api_input.put("reqTime", Utils.getCurrentDateAndTimeForAPI());
                api_input.put("request", request_json);

                // submit /documents POST service
                String url = "/pre-registration/" + baseVersion + "/document/documents";
                RestAssured.baseURI = baseUri;
                Response api_response = RestAssured
                        .given().relaxedHTTPSValidation()
                        .multiPart("file", new File(proofDocument.path))
                        .formParam("Document request", api_input)
                        .contentType("multipart/form-data")
                        .post(url);

                res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
                AddCallRecord(res);

                /* check for api status */
                if(api_response.getStatusCode() != 200){
                    extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
                    Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
                    Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
                }

                /* Error handling middleware */
                if(step.error != null && !step.error.isEmpty()){
                    ReadContext ctx = stepErrorMiddleware(step, res);
                }else{
                    ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(api_response.getBody().asString());
                    /* Response data parsing */
                    try {
                        data.persons.get(i).documents.get(j).doc_id = ctx.read("$['response'][0]['documentId']");
                    } catch (PathNotFoundException e){

                    }

                    /* Assertion policies execution */
                    if(step.asserts.size()>0){
                        for (assertion_policy assertion_type : step.asserts) {
                            switch (assertion_type) {
                                case DONT:
                                    break;

                                case API_CALL:
                                    Scenario.Step nstep = new Scenario.Step();
                                    nstep.name = "getDocument";
                                    nstep.index.add(i);
                                    CallRecord getAppRecord = getDocuments(nstep);

                                    HashMap<String, Object> hm = ResponseParser.addDocumentResponseParser(data.persons.get(i).documents.get(j), getAppRecord.response);

                                    extentTest.log((Status) hm.get("status"), hm.get("msg").toString());
                                    if (hm.get("status").equals(Status.FAIL)) {
                                        Utils.auditLog.severe(hm.get("msg").toString());
                                    } else {
                                        Utils.auditLog.info(hm.get("msg").toString());
                                    }
                                    Utils.auditLog.info("-----------------------------------------------------------------------------------------------------------------");

                                    Assert.assertEquals(hm.get("status"), Status.INFO, hm.get("msg").toString());
                                    break;

                                case STATUS:
                                    if(!(Boolean) ctx.read("$['status']")){
                                        extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                        Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                        Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                                    }
                                    break;

                                case DEFAULT:
                                    if(!(Boolean) ctx.read("$['status']")){
                                        extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                        Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                        Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                                    }
                                    break;

                                default:
                                    extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                                    Utils.auditLog.warning("Skipping assert "+assertion_type);
                                    break;
                            }
                        }
                    }
                }
            }
        }

        return res;
    }

    public static CallRecord deleteDocument(Scenario.Step step) {
        switch (step.variant) {
            case "DEFAULT":
                break;

            default:
                extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
                Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
                return new CallRecord();
        }
        int index = UtilsA.getPersonIndex(step);
        String url = "/pre-registration/" + baseVersion + "/document/documents";
        RestAssured.baseURI = baseUri;
        Response api_response = RestAssured.given()
                .queryParam("documentId", data.persons.get(index).documents.get(0).doc_id)
                .delete(url);

        CallRecord res = new CallRecord(url, step.name, data.persons.get(index).documents.get(0).doc_id, api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;

                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }

        return res;
    }

    public static CallRecord deleteDocumentsByPreRegID(Scenario.Step step) {
        switch (step.variant) {
            case "DEFAULT":
                break;

            default:
                extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
                Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
                return new CallRecord();
        }
        int index = UtilsA.getPersonIndex(step);
        String url = "/pre-registration/" + baseVersion + "/document/documents/byPreRegId";
        RestAssured.baseURI = baseUri;
        Response api_response = RestAssured.given()
                .queryParam("pre_registration_id", data.persons.get(index).pre_registration_id)
                .delete(url);

        CallRecord res = new CallRecord(url, step.name, data.persons.get(index).pre_registration_id, api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;

                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }

        return res;
    }

    public static CallRecord copyDocument(Scenario.Step step) {
        switch (step.variant) {
            case "DEFAULT":
                break;

            default:
                extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
                Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
                return new CallRecord();
        }
        if(step.index.size()!= 2){
            extentTest.log(Status.WARNING, "Skipping step as invalid step entry for step: "+step.name);
            Utils.auditLog.warning("Skipping step as invalid step entry for step: "+step.name);
            return new CallRecord();
        }
        String catCode = "POA";
        String destination_prereg_Id = data.persons.get(step.index.get(0)).pre_registration_id;
        String source_prereg_id = data.persons.get(step.index.get(1)).pre_registration_id;

        String url = "/pre-registration/" + baseVersion + "/document/documents/copy";
        RestAssured.baseURI = baseUri;
        Response api_response = RestAssured.given()
                .queryParam("catCode", catCode)
                .queryParam("destinationPreId", destination_prereg_Id)
                .queryParam("sourcePrId", source_prereg_id)
                .post(url);

        CallRecord res = new CallRecord(url, step.name, "Destination Pre-Reg ID: " + destination_prereg_Id + ", Source Pre-Reg ID: " + source_prereg_id, api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;

                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }

        return res;
    }

    public static CallRecord getDocuments(Scenario.Step step) {
        switch (step.variant) {
            case "DEFAULT":
                break;

            default:
                extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
                Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
                return new CallRecord();
        }

        int index = UtilsA.getPersonIndex(step);
        Persona person = data.persons.get(index);
        JsonObject inputData = new JsonObject();
        inputData.addProperty("pre_registration_id", person.pre_registration_id);

        String url = "/pre-registration/" + baseVersion + "/document/documents";
        RestAssured.baseURI = baseUri;
        Response api_response = RestAssured.given()
                .queryParam("pre_registration_id", person.pre_registration_id)
                .get(url);

        CallRecord res = new CallRecord(url, step.name, inputData.toString(), api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;

                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }

        return res;
    }
}