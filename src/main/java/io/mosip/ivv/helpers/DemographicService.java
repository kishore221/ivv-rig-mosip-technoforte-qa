/*
 *  Author: Sanjeev
 */
package main.java.io.mosip.ivv.helpers;

import static io.restassured.RestAssured.given;

import java.util.Date;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.jayway.jsonpath.PathNotFoundException;
import com.jayway.jsonpath.ReadContext;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import main.java.io.mosip.ivv.base.CallRecord;
import main.java.io.mosip.ivv.base.Persona;
import main.java.io.mosip.ivv.orchestrator.Scenario;
import main.java.io.mosip.ivv.orchestrator.Scenario.Data;
import main.java.io.mosip.ivv.utils.ResponseParser;
import main.java.io.mosip.ivv.utils.Utils;
import main.java.io.mosip.ivv.utils.UtilsA;

public class DemographicService extends Controller {

	public static JsonPath jsonFormatPath = null;
	
    public DemographicService(Data data) {
                super(data,extentTest);
                // TODO Auto-generated constructor stub
        }

    @SuppressWarnings({ "unchecked", "incomplete-switch", "serial", "unused" })
	public static CallRecord addApplication(Scenario.Step step) {
    	CallRecord res=null;
        int index = UtilsA.getPersonIndex(step);
    	Persona person = data.persons.get(index);
    	
        JSONObject identity_json = new JSONObject();
        identity_json.put("dateOfBirth", person.date_of_birth);
        identity_json.put("email", person.email);
        identity_json.put("phone", person.phone);
        identity_json.put("CNIENumber", person.cnie_number);
        identity_json.put("IDSchemaVersion", 1.0);
        identity_json.put("postalCode", person.postal_code);
        HashMap<String, String> demographic = new HashMap<>();
        demographic.put("fullName", person.name);
        demographic.put("gender", person.getGender());
        demographic.put("addressLine1", person.address_line_1);
        demographic.put("addressLine2", person.address_line_2);
        demographic.put("addressLine3", person.address_line_3);
        demographic.put("region", person.region);
        demographic.put("province", person.province);
        demographic.put("city", person.city);
        demographic.put("localAdministrativeAuthority", person.local_administrative_authority);

        switch(step.variant){

            case "invalidGender" :
                demographic.put("gender", data.globals.get("INVALID_GENDER"));
                break;

            case "invalidEmail" :
                identity_json.put("email", data.globals.get("INVALID_EMAIL"));
                break;

            case "invalidPhone" :
                identity_json.put("phone", data.globals.get("INVALID_PHONE"));
                break;

            case "DEFAULT":
                break;

            default:
                extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
                Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
                return new CallRecord();

        }


        demographic.forEach((k,v)-> identity_json.put(k, new JSONArray(){{
                add(new JSONObject(
                            new HashMap<String, String>(){{
                                put("language", "eng");
                                put("value", v);
                            }}
                        )
                    );
                }}
            )
        );
        JSONObject demographic_json = new JSONObject();
        demographic_json.put("identity", identity_json);

        JSONObject request_json = new JSONObject();
        request_json.put("preRegistrationId", "");
        request_json.put("langCode", "ENG");
        request_json.put("createdBy", person.phone);
        request_json.put("updatedBy", "");
        request_json.put("demographicDetails", demographic_json);
        request_json.put("createdDateTime", Utils.getCurrentDateAndTimeForAPI());
        request_json.put("updatedDateTime", "");


        JSONObject api_input = new JSONObject();
        api_input.put("id", "mosip.pre-registration.demographic.create");
        api_input.put("ver", "1.0");
        api_input.put("reqTime", Utils.getCurrentDateAndTimeForAPI());
        api_input.put("request", request_json);

        String url = "/pre-registration/" + baseVersion + "/demographic/applications";
        RestAssured.baseURI = baseUri;
        RestAssured.useRelaxedHTTPSValidation();
        Response api_response = (Response) given().contentType(ContentType.JSON).body(api_input).post(url);
        res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            ReadContext ctx = stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */
            try {
                data.persons.get(index).pre_registration_id = ctx.read("$['response'][0]['preRegistrationId']");
                data.persons.get(index).pre_registration_status_code = ctx.read("$['response'][0]['statusCode']");
            } catch (PathNotFoundException e){

            }

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;
                            
                        case API_CALL:
                            Scenario.Step nstep = new Scenario.Step();
                            nstep.name = "getApplication";
                            nstep.index.add(index);
                            CallRecord getAppRecord = getApplication(nstep);
                            
                            HashMap <String, Object> hm = ResponseParser.addApplicationResponceParser(data.persons.get(index), getAppRecord.response);

                            
                            extentTest.log((Status) hm.get("status"), hm.get("msg").toString());
                            if (hm.get("status").equals(Status.FAIL)) {
                                Utils.auditLog.severe(hm.get("msg").toString());
                            } else {
                                Utils.auditLog.info(hm.get("msg").toString());
                            }
                            Utils.auditLog.info("-----------------------------------------------------------------------------------------------------------------");

                            Assert.assertEquals(hm.get("status"), Status.INFO, hm.get("msg").toString());
                            break;
                            
                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }

        return res;
    }

    @SuppressWarnings({ "unchecked", "incomplete-switch", "serial", "unused" })
	public static CallRecord addApplicationAll(Scenario.Step step) {
    	CallRecord res=null;
    	for (int i = 0; i < data.persons.size(); i++) {

            Persona person = data.persons.get(i);

            JSONObject identity_json = new JSONObject();
            identity_json.put("dateOfBirth", person.date_of_birth);
            identity_json.put("email", person.email);
            identity_json.put("phone", person.phone);
            identity_json.put("CNIENumber", person.cnie_number);
            identity_json.put("IDSchemaVersion", 1.0);
            identity_json.put("postalCode", person.postal_code);
            HashMap<String, String> demographic = new HashMap<>();
            demographic.put("fullName", person.name);
            demographic.put("gender", person.getGender());
            demographic.put("addressLine1", person.address_line_1);
            demographic.put("addressLine2", person.address_line_2);
            demographic.put("addressLine3", person.address_line_3);
            demographic.put("region", person.region);
            demographic.put("province", person.province);
            demographic.put("city", person.city);
            demographic.put("localAdministrativeAuthority", person.local_administrative_authority);

            switch (step.variant) {

                case "invalidGender":
                    demographic.put("gender", data.globals.get("INVALID_GENDER"));
                    break;

                case "invalidEmail":
                    identity_json.put("gender", data.globals.get("INVALID_EMAIL"));
                    break;

                case "invalidPhone":
                    identity_json.put("gender", data.globals.get("INVALID_PHONE"));
                    break;

                case "DEFAULT":
                    break;

                default:
                    extentTest.log(Status.WARNING, "Skipping step " + step.name + " as variant " + step.variant + " not found");
                    Utils.auditLog.warning("Skipping step " + step.name + " as variant " + step.variant + " not found");
                    return new CallRecord();

            }


            demographic.forEach((k, v) -> identity_json.put(k, new JSONArray() {{
                        add(new JSONObject(
                                        new HashMap<String, String>() {{
                                            put("language", "eng");
                                            put("value", v);
                                        }}
                                )
                        );
                    }}
                    )
            );
            JSONObject demographic_json = new JSONObject();
            demographic_json.put("identity", identity_json);

            JSONObject request_json = new JSONObject();
            request_json.put("preRegistrationId", "");
            request_json.put("langCode", "ENG");
            request_json.put("createdBy", person.phone);
            request_json.put("updatedBy", "");
            request_json.put("demographicDetails", demographic_json);
            request_json.put("createdDateTime", Utils.getCurrentDateAndTimeForAPI());
            request_json.put("updatedDateTime", "");


            JSONObject api_input = new JSONObject();
            api_input.put("id", "mosip.pre-registration.demographic.create");
            api_input.put("ver", "1.0");
            api_input.put("reqTime", Utils.getCurrentDateAndTimeForAPI());
            api_input.put("request", request_json);

            String url = "/pre-registration/" + baseVersion + "/demographic/applications";
            RestAssured.baseURI = baseUri;
            RestAssured.useRelaxedHTTPSValidation();
            Response api_response = (Response) given().contentType(ContentType.JSON).body(api_input).post(url);

            res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
            AddCallRecord(res);

            /* check for api status */
            if (api_response.getStatusCode() != 200) {
                extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected [" + 200 + "], actual[" + api_response.getStatusCode() + "]");
                Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
                Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
            }

            /* Error handling middleware */
            if (step.error != null && !step.error.isEmpty()) {
                ReadContext ctx = stepErrorMiddleware(step, res);
            } else {
                ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(api_response.getBody().asString());
                /* Response data parsing */
                try {
                    data.persons.get(i).pre_registration_id = ctx.read("$['response'][0]['preRegistrationId']");
                    data.persons.get(i).pre_registration_status_code = ctx.read("$['response'][0]['statusCode']");
                } catch (PathNotFoundException e){

                }

                /* Assertion policies execution */
                if (step.asserts.size() > 0) {
                    for (assertion_policy assertion_type : step.asserts) {
                        switch (assertion_type) {
                            case DONT:
                                break;
                                
                            /*case API_CALL:
                                Scenario.Step nstep = new Scenario.Step();
                                nstep.name = "getApplication";
                                nstep.index.add(i);
                                CallRecord getAppRecord = getApplication(nstep);
                                
                                HashMap <String, Object> hm = ResponseParser.addApplicationResponceParser(data.persons.get(i), getAppRecord.response);

                                
                                extentTest.log((Status) hm.get("status"), hm.get("msg").toString());
                                if (hm.get("status").equals(Status.FAIL)) {
                                    Utils.auditLog.severe(hm.get("msg").toString());
                                } else {
                                    Utils.auditLog.info(hm.get("msg").toString());
                                }
                                Utils.auditLog.info("-----------------------------------------------------------------------------------------------------------------");

                                Assert.assertEquals(hm.get("status"), Status.INFO, hm.get("msg").toString());
                                break;*/
                                
                            case STATUS:
                                if (!(Boolean) ctx.read("$['status']")) {
                                    extentTest.log(Status.FAIL, "Assert via STATUS, expected [" + true + "], actual[" + ctx.read("$['status']") + "]");
                                    Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                    Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                                }
                                break;

                            case DEFAULT:
                                if (!(Boolean) ctx.read("$['status']")) {
                                    extentTest.log(Status.FAIL, "Assert via STATUS, expected [" + true + "], actual[" + ctx.read("$['status']") + "]");
                                    Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                    Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                                }
                                break;

                            default:
                                extentTest.log(Status.WARNING, "Skipping assert " + assertion_type);
                                Utils.auditLog.warning("Skipping assert " + assertion_type);
                                break;
                        }
                    }
                }
            }
        }

        return res;
    }

    
    @SuppressWarnings({ "unchecked", "serial", "incomplete-switch", "unused" })
    public static CallRecord updateApplication(Scenario.Step step) {
    	CallRecord res=null;
        int index = UtilsA.getPersonIndex(step);
    	Persona person = data.persons.get(index);
    	
        JSONObject identity_json = new JSONObject();
        
        identity_json.put("dateOfBirth", UtilsA.updateDOB(person));
        identity_json.put("email", person.email);
        identity_json.put("phone", person.phone);
        identity_json.put("CNIENumber", person.cnie_number);
        identity_json.put("IDSchemaVersion", 1.0);
        identity_json.put("postalCode", person.postal_code);
        HashMap<String, String> demographic = new HashMap<>();
        demographic.put("fullName", person.name);
        demographic.put("gender", person.getGender());
        demographic.put("addressLine1", person.address_line_1);
        demographic.put("addressLine2", person.address_line_2);
        demographic.put("addressLine3", person.address_line_3);
        demographic.put("region", person.region);
        demographic.put("province", person.province);
        demographic.put("city", person.city);
        demographic.put("localAdministrativeAuthority", person.local_administrative_authority);

        switch(step.variant){

            case "invalidGender" :
                demographic.put("gender", data.globals.get("INVALID_GENDER"));
                break;

            case "invalidEmail" :
                identity_json.put("email", data.globals.get("INVALID_EMAIL"));
                break;

            case "invalidPhone" :
                identity_json.put("phone", data.globals.get("INVALID_PHONE"));
                break;

            case "DEFAULT":
                break;

            default:
                extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
                Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
                return new CallRecord();

        }


        demographic.forEach((k,v)-> identity_json.put(k, new JSONArray(){{
                add(new JSONObject(
                            new HashMap<String, String>(){{
                                put("language", "eng");
                                put("value", v);
                            }}
                        )
                    );
                }}
            )
        );
        JSONObject demographic_json = new JSONObject();
        demographic_json.put("identity", identity_json);

        JSONObject request_json = new JSONObject();
        request_json.put("preRegistrationId", person.pre_registration_id);
        request_json.put("langCode", "ENG");
        request_json.put("createdBy", person.phone);
        request_json.put("updatedBy", person.phone);
        request_json.put("demographicDetails", demographic_json);
        request_json.put("createdDateTime", Utils.getCurrentDateAndTimeForAPI());
        request_json.put("updatedDateTime", Utils.getCurrentDateAndTimeForAPI());


        JSONObject api_input = new JSONObject();
        api_input.put("id", "mosip.pre-registration.demographic.create");
        api_input.put("ver", "1.0");
        api_input.put("reqTime", Utils.getCurrentDateAndTimeForAPI());
        api_input.put("request", request_json);

        String url = "/pre-registration/" + baseVersion + "/demographic/applications";
        RestAssured.baseURI = baseUri;
        RestAssured.useRelaxedHTTPSValidation();
        Response api_response = (Response) given()
        		.contentType(ContentType.JSON).body(api_input).post(url);
        res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            ReadContext ctx = stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */
            try {
                data.persons.get(index).pre_registration_id = ctx.read("$['response'][0]['preRegistrationId']");
                data.persons.get(index).pre_registration_status_code = ctx.read("$['response'][0]['statusCode']");
            } catch (PathNotFoundException e){

            }

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;

                        case API_CALL:
                            Scenario.Step nstep = new Scenario.Step();
                            nstep.name = "updateApplication";
                            nstep.index.add(index);
                            CallRecord getAppRecord = getApplication(nstep);
                            
                            HashMap <String, Object> hm = ResponseParser.updateApplicationResponceParser(data.persons.get(index), getAppRecord.response);

                            
                            extentTest.log((Status) hm.get("status"), hm.get("msg").toString());
                            if (hm.get("status").equals(Status.FAIL)) {
                                Utils.auditLog.severe(hm.get("msg").toString());
                            } else {
                                Utils.auditLog.info(hm.get("msg").toString());
                            }
                            Utils.auditLog.info("-----------------------------------------------------------------------------------------------------------------");

                            Assert.assertEquals(hm.get("status"), Status.INFO, hm.get("msg").toString());
                            break;
                            
                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }

        return res;
    }

	

	@SuppressWarnings("unused")
	public static CallRecord deleteApplication(Scenario.Step step) {
        switch (step.variant) {

            case "DEFAULT":
                break;

            default:
                extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
                Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
                return new CallRecord();
        }
        String pre_registration_id = data.persons.get(0).pre_registration_id;
        int index = UtilsA.getPersonIndex(step);
        Persona person = data.persons.get(index);

        String url = "/pre-registration/" + baseVersion + "/demographic/applications";
		RestAssured.baseURI = baseUri;
    	api_response = RestAssured.given()
				.queryParam("pre_registration_id", person.pre_registration_id)
                .delete(url);
    	//System.out.println("API RESPONSE***"+api_response.getBody().prettyPrint());
		CallRecord res = new CallRecord(url, step.name, person.pre_registration_id, api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            ReadContext ctx = stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;

                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }

        return res;

	}
    
	@SuppressWarnings({ "unchecked", "unused" })
	public static CallRecord getApplication(Scenario.Step step) {
        switch (step.variant) {
            case "DEFAULT":
                break;

            default:
                extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
                Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
                return new CallRecord();
        }
        int index = UtilsA.getPersonIndex(step);
    	Persona person = data.persons.get(index);
		
		JSONObject api_input = new JSONObject();
        api_input.put("pre_registration_id", person.pre_registration_id);

        String url = "/pre-registration/" + baseVersion + "/demographic/applications/details";
        RestAssured.baseURI = baseUri;
        RestAssured.useRelaxedHTTPSValidation();
        Response api_response = (Response) given()
                .queryParam("pre_registration_id", person.pre_registration_id)
                .get(url);
        CallRecord res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            ReadContext ctx = stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;

                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }

        return res;
    }


}
