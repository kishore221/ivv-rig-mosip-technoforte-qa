package main.java.io.mosip.ivv.helpers;

import com.aventstack.extentreports.Status;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import main.java.io.mosip.ivv.base.BaseHelper;
import main.java.io.mosip.ivv.base.CallRecord;

import static io.restassured.RestAssured.given;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import main.java.io.mosip.ivv.orchestrator.Scenario;
import com.aventstack.extentreports.ExtentTest;
import main.java.io.mosip.ivv.utils.Utils;

import javax.swing.text.Document;

public class Controller extends BaseHelper {
    public static Properties prop;
    public static String CONFIG_FILE_PATH = System.getProperty("user.dir")+"/config.properties";
    public static Response api_response;
    public static Scenario.Data data;
    public static String baseUri = "";
    public static String baseVersion = "";

    public Controller(Scenario.Data dataSet, ExtentTest ex) {
        baseUri = BaseHelper.baseUri;
        baseVersion = BaseHelper.baseVersion;
        data = dataSet;
        calls = new ArrayList<CallRecord>();
        extentTest = ex;

        try {
            prop = new Properties();
            FileInputStream ip = new FileInputStream(CONFIG_FILE_PATH);
            prop.load(ip);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Use relaxed HTTP validation with protocol
        RestAssured.useRelaxedHTTPSValidation();
    }

    @Override
    public void SetData(String json) {
        // initialize data
    }

    public String getCallRecords() {
        String listString = "";
        for (CallRecord call : calls) {
            listString += "Method " + call.method + "\n";
            listString += "Inputdata " + call.input_data + "\n";
            listString += "Response " + call.response.getBody().print() + "\n";
            listString += "Status " + call.status + "\n";
            listString += "------------------------------------------" + "\n";
        }
        return listString;
    }

    public Controller login(login_variations variation, assertion_policy assertionPolicy) {
        // variation {EMAIL, PHONE, ...} TODO caps all
        // assertionPolicy {DONT, STATUS, API_CALL, AUDIT_LOG, DB_VARIANT, ...}

        switch (variation) {
            case EMAIL:

                loginWithEmail();

                break;
            case PHONE:
                loginWithPhone();

                break;
            default:
                break;
        }
        return this;
    }

    private void loginWithEmail() {
        // create json for login api call
        RestAssured.baseURI = baseUri;
        api_response = (Response) given().param("userid", new Object[]{data.user.userid}).param("otp", new Object[]{data.user.otp}).when().get("/v1.0/authenticate/useridOTP",
                new Object[0]).body();
    }

    private void loginWithPhone() {
        //TODO
        RestAssured.baseURI = baseUri;
        api_response = (Response) given().param("userid", new Object[]{data.user.userid}).param("otp", new Object[]{data.user.otp}).when().get("/v1.0/authenticate/useridOTP",
                new Object[0]).body();
    }

    public Controller run(Scenario.Step step) {
        CallRecord record = null;
        switch (step.name) {
            /* Login */

            /* AddApplication */
            case "AddApplication":
                record = DemographicService.addApplication(step);
                break;

            /* AddApplicationAll */
            case "AddApplicationAll":
            	record = DemographicService.addApplicationAll(step);
                break;

            /* GetApplication */
            case "GetApplication":
                record = DemographicService.getApplication(step);
                break;

            /* DiscardApplication */
            case "DiscardApplication":
                record = DemographicService.deleteApplication(step);
                break;

            /* UpdateDemographics */
            case "UpdateDemographics":
                record = DemographicService.updateApplication(step);
                break;

            /* AddDocuments */
            case "AddDocuments":
                record = DocumentService.addDocument(step);
                break;

            /* AddDocumentsAll */
            case "AddDocumentsAll":
                record = DocumentService.addDocumentAll(step);
                break;

            /* GetDocument */
            case "GetDocument":
                record = DocumentService.getDocuments(step);
                break;

            /* DeleteDocument */
            case "DeleteDocument":
                record = DocumentService.deleteDocument(step);
                break;

            /* DeleteDocumentsByPreRegID */
            case "DeleteDocumentsByPreRegID":
                record = DocumentService.deleteDocumentsByPreRegID(step);
                break;

            /* CopyDocument */
            case "CopyDocument":
                record = DocumentService.copyDocument(step);
                break;

            /* BookAppointment */
            case "BookAppointment":
                record = BookingService.bookAppointment(step);
                break;

            /* BookAppointmentAll */
            case "BookAppointmentAll":
                record = BookingService.bookAppointmentAll(step);
                break;

            /* ReBookAppointment */
            case "ReBookAppointment":
                record = BookingService.reBookAppointment(step);
                break;

            /* GetAppointment */
            case "GetAppointment":
                record = BookingService.getAppointment(step);
                break;

            /* GetAvailableSlots */
            case "GetAvailableSlots":
                record = BookingService.getAvailableSlots(step);
                break;

            /* CancelAppointment */
            case "CancelAppointment":
                record = BookingService.cancelAppointment(step);
                break;

            /* GenerateQRCode */
            case "GenerateQRCode":
                //record = NotificationService.generateQRCode(step);
                break;

            /* Notify */
            case "Notify":
                //record = NotificationService.notify(step);
                break;
            
            /*GenerteVIN*/
            case "GenerateVIN":
            	record = ResidentPortalService.generateVIN(step);
            	break;
            
            /*RevokeVIN*/
            case "RevokeVIN":
            	record = ResidentPortalService.revokeVIN(step);
            	break;
            
            /*LockBiometric*/
            case "LockBiometric":
            	record = ResidentPortalService.lockBiometric(step);
            	break;
            
            /*UnlockBiometric*/
            case "UnlockBiometric":
            	record = ResidentPortalService.unlockBiometric(step);
            	break;
            	
            /*UpdateDemographic*/
            case "UpdateDemographic":
            	record = ResidentPortalService.updateDemographic(step);
            	break;	
            
            default:
                extentTest.log(Status.WARNING, "Skipping step " + step.name);
                Utils.auditLog.warning("Skipping step " + step.name);
                Utils.auditLog.info("-----------------------------------------------------------------------------------------------------------------");
                break;

        }

        return this;
    }

}