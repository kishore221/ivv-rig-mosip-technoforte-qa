package main.java.io.mosip.ivv.helpers;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import main.java.io.mosip.ivv.base.CallRecord;
import main.java.io.mosip.ivv.base.Persona;
import main.java.io.mosip.ivv.orchestrator.Scenario;
import main.java.io.mosip.ivv.utils.Utils;
import main.java.io.mosip.ivv.utils.UtilsA;
import org.json.simple.JSONObject;
import org.testng.Assert;

import static io.restassured.RestAssured.given;

public class NotificationService extends Controller {

    public NotificationService(Scenario.Data dataSet, ExtentTest ex) {
        super(dataSet, ex);
    }

    public static CallRecord generateQRCode(Scenario.Step step) {
        switch (step.variant) {

            case "DEFAULT":
                break;

            default:
                extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
                Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
                return new CallRecord();
        }
        int index = UtilsA.getPersonIndex(step);
        Persona person = data.persons.get(index);

        JSONObject api_input = new JSONObject();
        api_input.put("name", person.name);
        api_input.put("preId", person.pre_registration_id);
        api_input.put("appointmentDate", "");
        api_input.put("appointmentTime", "");
        api_input.put("mobNum", person.phone);
        api_input.put("emailID", person.email);

        String url = "/pre-registration/"+baseVersion + "/notification/generateQRCode";
        RestAssured.baseURI = baseUri;
        RestAssured.useRelaxedHTTPSValidation();
        RestAssured.useRelaxedHTTPSValidation();
        Response api_response = (Response) given()
                .contentType(ContentType.JSON)
                .body(api_input)
                .post(url);
        CallRecord res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;

                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }

        return res;
    }

    public static CallRecord notify(Scenario.Step step) {
        switch (step.variant) {

            case "DEFAULT":
                break;

            default:
                extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
                Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
                return new CallRecord();
        }
        int index = UtilsA.getPersonIndex(step);
        Persona person = data.persons.get(index);

        JSONObject api_input = new JSONObject();
        api_input.put("name", person.name);
        api_input.put("preId", person.pre_registration_id);
        api_input.put("appointmentDate", "");
        api_input.put("appointmentTime", "");
        api_input.put("mobNum", person.phone);
        api_input.put("emailID", person.email);

        String url = "/pre-registration/"+baseVersion + "/notification/notify";
        RestAssured.baseURI = baseUri;
        RestAssured.useRelaxedHTTPSValidation();
        RestAssured.useRelaxedHTTPSValidation();
        Response api_response = (Response) given()
                .contentType(ContentType.JSON)
                .body(api_input)
                .post("/pre-registration/"+baseVersion + "/notification/notify");
        CallRecord res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;

                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }

        return res;
    }

}
