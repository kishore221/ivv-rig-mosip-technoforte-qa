package main.java.io.mosip.ivv.helpers;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import org.json.simple.JSONObject;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.jayway.jsonpath.PathNotFoundException;
import com.jayway.jsonpath.ReadContext;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import main.java.io.mosip.ivv.base.CallRecord;
import main.java.io.mosip.ivv.base.Persona;
import main.java.io.mosip.ivv.base.BaseHelper.assertion_policy;
import main.java.io.mosip.ivv.orchestrator.Scenario;
import main.java.io.mosip.ivv.orchestrator.Scenario.Data;
import main.java.io.mosip.ivv.utils.ResponseParser;
import main.java.io.mosip.ivv.utils.Utils;
import main.java.io.mosip.ivv.utils.UtilsA;

public class ResidentPortalService extends Controller{

	public ResidentPortalService(Data data) {
        super(data, extentTest);
	}
	
	
	public static CallRecord generateVIN(Scenario.Step step) {
		CallRecord res = null;
		boolean expectedStatus = true;
		int index = UtilsA.getPersonIndex(step);
    	Persona person = data.persons.get(index);
        JSONObject data_json = new JSONObject();
        
        switch(step.variant){

        	case "InvalidUIN" :
        	data_json.put("uin", data.globals.get("INVALID_UIN"));
        	expectedStatus = false;
            break;
            
        	case "Revoked" :
        	data_json.put("uin", data.globals.get("INVALID_UIN"));
        	data_json.put("uin","5432");
        	expectedStatus = false;
            break;

        	case "invalidEmail" :
	        	data_json.put("email", data.globals.get("INVALID_EMAIL"));
	            break;
	
	        case "invalidPhone" :
	        	data_json.put("phone", data.globals.get("INVALID_PHONE"));
	            break;
	
	        case "DEFAULT":
	        	data_json.put("uin","2345");
	            break;
	
	        default:
	            extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
	            Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
	            return new CallRecord();
	
	    }

        JSONObject request_json = new JSONObject();
        request_json.put("langCode", "ENG");
        request_json.put("createdBy", person.phone);
        request_json.put("updatedBy", "");
        request_json.put("identity", data_json);
        request_json.put("createdDateTime", Utils.getCurrentDateAndTimeForAPI());
        request_json.put("updatedDateTime", "");


        JSONObject api_input = new JSONObject();
        api_input.put("id", "mosip.pre-registration.demographic.create");
        api_input.put("ver", "1.0");
        api_input.put("reqTime", Utils.getCurrentDateAndTimeForAPI());
        api_input.put("request", request_json);
        
        String url = "http://localhost:51310/mosip/vin/generate";
        
        
        RestAssured.baseURI = baseUri;
        RestAssured.useRelaxedHTTPSValidation();
        Response api_response = (Response) given().contentType(ContentType.JSON).body(api_input).post(url);
        res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            ReadContext ctx = stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */
            try {
            	//Long vin= ctx.read("$['vin']");
            	//String VID = vin.toString();
            	//data.persons.get(index).VIDs.add(VID); 
                //System.out.println("kunal checking VIDs :"+ctx.read("$['vin']"));
                
            } catch (PathNotFoundException e){

            }

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;
                        /*    
                        case API_CALL:
                            Scenario.Step nstep = new Scenario.Step();
                            nstep.name = "getApplication";
                            nstep.index.add(index);
                            CallRecord getAppRecord = getApplication(nstep);
                            
                            HashMap <String, Object> hm = ResponseParser.addApplicationResponceParser(data.persons.get(index), getAppRecord.response);

                            
                            extentTest.log((Status) hm.get("status"), hm.get("msg").toString());
                            if (hm.get("status").equals(Status.FAIL)) {
                                Utils.auditLog.severe(hm.get("msg").toString());
                            } else {
                                Utils.auditLog.info(hm.get("msg").toString());
                            }
                            Utils.auditLog.info("-----------------------------------------------------------------------------------------------------------------");

                            Assert.assertEquals(hm.get("status"), Status.INFO, hm.get("msg").toString());
                            break;
                        */    
                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean)expectedStatus, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }
        return res;
    }
	
	public static CallRecord revokeVIN(Scenario.Step step) {
		CallRecord res = null;
		boolean expectedStatus = true;
		int index = UtilsA.getPersonIndex(step);
    	Persona person = data.persons.get(index);
        JSONObject data_json = new JSONObject();
        
        switch(step.variant){

        	case "invalidEmail" :
	        	data_json.put("email", data.globals.get("INVALID_EMAIL"));
	            break;
	            
	        case "invalidPhone" :{
	        	data_json.put("phone", data.globals.get("INVALID_PHONE"));
	            break;
	        }
	        case "DEFAULT":
	        	data_json.put("uin","5432");
	            break;
	
	        default:
	            extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
	            Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
	            return new CallRecord();
	
	    }

        JSONObject request_json = new JSONObject();
        request_json.put("langCode", "ENG");
        request_json.put("createdBy", person.phone);
        request_json.put("updatedBy", "");
        request_json.put("identity", data_json);
        request_json.put("createdDateTime", Utils.getCurrentDateAndTimeForAPI());
        request_json.put("updatedDateTime", "");


        JSONObject api_input = new JSONObject();
        api_input.put("id", "mosip.pre-registration.demographic.create");
        api_input.put("ver", "1.0");
        api_input.put("reqTime", Utils.getCurrentDateAndTimeForAPI());
        api_input.put("request", request_json);
        
        String url = "http://localhost:51310/mosip/vin/revoke";
        
        
        RestAssured.baseURI = baseUri;
        RestAssured.useRelaxedHTTPSValidation();
        Response api_response = (Response) given().contentType(ContentType.JSON).body(api_input).post(url);
        res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);
        //System.out.println("kunal testing :"+api_response);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            ReadContext ctx = stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */
            try {
            	//Long vin= ctx.read("$['vin']");
            	//String VID = vin.toString();
            	//data.persons.get(index).VIDs.add(VID); 
                //System.out.println("kunal checking VIDs :"+ctx.read("$['vin']"));
                
            } catch (PathNotFoundException e){

            }

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;
                        /*    
                        case API_CALL:
                            Scenario.Step nstep = new Scenario.Step();
                            nstep.name = "getApplication";
                            nstep.index.add(index);
                            CallRecord getAppRecord = getApplication(nstep);
                            
                            HashMap <String, Object> hm = ResponseParser.addApplicationResponceParser(data.persons.get(index), getAppRecord.response);

                            
                            extentTest.log((Status) hm.get("status"), hm.get("msg").toString());
                            if (hm.get("status").equals(Status.FAIL)) {
                                Utils.auditLog.severe(hm.get("msg").toString());
                            } else {
                                Utils.auditLog.info(hm.get("msg").toString());
                            }
                            Utils.auditLog.info("-----------------------------------------------------------------------------------------------------------------");

                            Assert.assertEquals(hm.get("status"), Status.INFO, hm.get("msg").toString());
                            break;
                        */    
                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean)expectedStatus, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }
        return res;
	}
	public static CallRecord lockBiometric(Scenario.Step step) {
		CallRecord res = null;
		boolean expectedStatus = true;
		int index = UtilsA.getPersonIndex(step);
    	Persona person = data.persons.get(index);
        //JSONObject data_json = new JSONObject();
        JSONObject request_json = new JSONObject();
        switch(step.variant){

        	case "invalidOTP" :
        		request_json.put("enterOTP", "3748");
        		expectedStatus = false;
        		break;
            
	        case "DEFAULT":
	        	request_json.put("enterOTP", "6543");
	            break;
	
	        default:
	            extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
	            Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
	            return new CallRecord();
	
	    }

        request_json.put("uin", "2345");
        request_json.put("sentOTP", "6543");
        request_json.put("langCode", "ENG");
        request_json.put("createdBy", person.phone);
        request_json.put("updatedBy", "");
        //request_json.put("identity", data_json);
        request_json.put("createdDateTime", Utils.getCurrentDateAndTimeForAPI());
        request_json.put("updatedDateTime", "");


        JSONObject api_input = new JSONObject();
        api_input.put("id", "mosip.pre-registration.demographic.create");
        api_input.put("ver", "1.0");
        api_input.put("reqTime", Utils.getCurrentDateAndTimeForAPI());
        api_input.put("request", request_json);
        
        String url = "http://localhost:51310/mosip/uin/lockBiometrics";
        
        
        RestAssured.baseURI = baseUri;
        RestAssured.useRelaxedHTTPSValidation();
        Response api_response = (Response) given().contentType(ContentType.JSON).body(api_input).post(url);
        res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);
        //System.out.println("kunal testing :"+api_response);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            ReadContext ctx = stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */
            try {
            	//Long vin= ctx.read("$['vin']");
            	//String VID = vin.toString();
            	//data.persons.get(index).VIDs.add(VID); 
                //System.out.println("kunal checking VIDs :"+ctx.read("$['vin']"));
                
            } catch (PathNotFoundException e){

            }

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;
                        /*    
                        case API_CALL:
                            Scenario.Step nstep = new Scenario.Step();
                            nstep.name = "getApplication";
                            nstep.index.add(index);
                            CallRecord getAppRecord = getApplication(nstep);
                            
                            HashMap <String, Object> hm = ResponseParser.addApplicationResponceParser(data.persons.get(index), getAppRecord.response);

                            
                            extentTest.log((Status) hm.get("status"), hm.get("msg").toString());
                            if (hm.get("status").equals(Status.FAIL)) {
                                Utils.auditLog.severe(hm.get("msg").toString());
                            } else {
                                Utils.auditLog.info(hm.get("msg").toString());
                            }
                            Utils.auditLog.info("-----------------------------------------------------------------------------------------------------------------");

                            Assert.assertEquals(hm.get("status"), Status.INFO, hm.get("msg").toString());
                            break;
                        */    
                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean)expectedStatus, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }
		return res;
	}
	public static CallRecord unlockBiometric(Scenario.Step step) {
		CallRecord res = null;
		boolean expectedStatus = true;
		int index = UtilsA.getPersonIndex(step);
    	Persona person = data.persons.get(index);
        //JSONObject data_json = new JSONObject();
        JSONObject request_json = new JSONObject();
        switch(step.variant){

        	case "invalidOTP" :
        		request_json.put("enterOTP", "3748");
        		expectedStatus  = false;
        		break;
            
	        case "DEFAULT":
	        	request_json.put("enterOTP", "6543");
	            break;
	
	        default:
	            extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
	            Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
	            return new CallRecord();
	
	    }

        request_json.put("uin", "2345");
        request_json.put("sentOTP", "6543");
        request_json.put("langCode", "ENG");
        request_json.put("createdBy", person.phone);
        request_json.put("updatedBy", "");
        //request_json.put("identity", data_json);
        request_json.put("createdDateTime", Utils.getCurrentDateAndTimeForAPI());
        request_json.put("updatedDateTime", "");


        JSONObject api_input = new JSONObject();
        api_input.put("id", "mosip.pre-registration.demographic.create");
        api_input.put("ver", "1.0");
        api_input.put("reqTime", Utils.getCurrentDateAndTimeForAPI());
        api_input.put("request", request_json);
        
        String url = "http://localhost:51310/mosip/uin/unlockBiometrics";
        
        
        RestAssured.baseURI = baseUri;
        RestAssured.useRelaxedHTTPSValidation();
        Response api_response = (Response) given().contentType(ContentType.JSON).body(api_input).post(url);
        res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);
        //System.out.println("kunal testing :"+api_response);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            ReadContext ctx = stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */
            try {
            	//Long vin= ctx.read("$['vin']");
            	//String VID = vin.toString();
            	//data.persons.get(index).VIDs.add(VID); 
                //System.out.println("kunal checking VIDs :"+ctx.read("$['vin']"));
                
            } catch (PathNotFoundException e){

            }

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;
                        /*    
                        case API_CALL:
                            Scenario.Step nstep = new Scenario.Step();
                            nstep.name = "getApplication";
                            nstep.index.add(index);
                            CallRecord getAppRecord = getApplication(nstep);
                            
                            HashMap <String, Object> hm = ResponseParser.addApplicationResponceParser(data.persons.get(index), getAppRecord.response);

                            
                            extentTest.log((Status) hm.get("status"), hm.get("msg").toString());
                            if (hm.get("status").equals(Status.FAIL)) {
                                Utils.auditLog.severe(hm.get("msg").toString());
                            } else {
                                Utils.auditLog.info(hm.get("msg").toString());
                            }
                            Utils.auditLog.info("-----------------------------------------------------------------------------------------------------------------");

                            Assert.assertEquals(hm.get("status"), Status.INFO, hm.get("msg").toString());
                            break;
                        */    
                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean)expectedStatus, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }

		return res;
	}
	public static CallRecord requestReprint(Scenario.Step step) {
		CallRecord res = null;
		boolean expectedStatus = true;
		int index = UtilsA.getPersonIndex(step);
    	Persona person = data.persons.get(index);
        JSONObject data_json = new JSONObject();
        
        switch(step.variant){

        	case "invalidEmail" :
	        	data_json.put("email", data.globals.get("INVALID_EMAIL"));
	            break;
	            
	        case "invalidPhone" :{
	        	data_json.put("phone", data.globals.get("INVALID_PHONE"));
	            break;
	        }
	        case "DEFAULT":
	        	data_json.put("uin","2345");
	            break;
	
	        default:
	            extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
	            Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
	            return new CallRecord();
	
	    }

        JSONObject request_json = new JSONObject();
        request_json.put("langCode", "ENG");
        request_json.put("createdBy", person.phone);
        request_json.put("updatedBy", "");
        request_json.put("identity", data_json);
        request_json.put("createdDateTime", Utils.getCurrentDateAndTimeForAPI());
        request_json.put("updatedDateTime", "");


        JSONObject api_input = new JSONObject();
        api_input.put("id", "mosip.pre-registration.demographic.create");
        api_input.put("ver", "1.0");
        api_input.put("reqTime", Utils.getCurrentDateAndTimeForAPI());
        api_input.put("request", request_json);
        
        String url = "http://localhost:51310/mosip/uin/reprint";
        
        
        RestAssured.baseURI = baseUri;
        RestAssured.useRelaxedHTTPSValidation();
        Response api_response = (Response) given().contentType(ContentType.JSON).body(api_input).post(url);
        res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);
        //System.out.println("kunal testing :"+api_response);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            ReadContext ctx = stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */
            try {
            	//Long vin= ctx.read("$['vin']");
            	//String VID = vin.toString();
            	//data.persons.get(index).VIDs.add(VID); 
                //System.out.println("kunal checking VIDs :"+ctx.read("$['vin']"));
                
            } catch (PathNotFoundException e){

            }

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;
                        /*    
                        case API_CALL:
                            Scenario.Step nstep = new Scenario.Step();
                            nstep.name = "getApplication";
                            nstep.index.add(index);
                            CallRecord getAppRecord = getApplication(nstep);
                            
                            HashMap <String, Object> hm = ResponseParser.addApplicationResponceParser(data.persons.get(index), getAppRecord.response);

                            
                            extentTest.log((Status) hm.get("status"), hm.get("msg").toString());
                            if (hm.get("status").equals(Status.FAIL)) {
                                Utils.auditLog.severe(hm.get("msg").toString());
                            } else {
                                Utils.auditLog.info(hm.get("msg").toString());
                            }
                            Utils.auditLog.info("-----------------------------------------------------------------------------------------------------------------");

                            Assert.assertEquals(hm.get("status"), Status.INFO, hm.get("msg").toString());
                            break;
                        */    
                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean)expectedStatus, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }

		return res;
	}
	public static CallRecord updateDemographic(Scenario.Step step) {
		CallRecord res = null;
		boolean expectedStatus = true;
		int index = UtilsA.getPersonIndex(step);
    	Persona person = data.persons.get(index);
        JSONObject data_json = new JSONObject();
        
        switch(step.variant){

        case "InvalidUIN" :
        	data_json.put("uin", data.globals.get("INVALID_UIN"));
        	expectedStatus = false;
            break;
            
	        case "invalidEmail" :
	        	data_json.put("email", data.globals.get("INVALID_EMAIL"));
	            break;
	
	        case "invalidPhone" :
	        	data_json.put("phone", data.globals.get("INVALID_PHONE"));
	            break;
	
	        case "DEFAULT":
	        	data_json.put("uin","2345");
	            break;
	
	        default:
	            extentTest.log(Status.WARNING, "Skipping step "+step.name+" as variant "+step.variant+" not found");
	            Utils.auditLog.warning("Skipping step "+step.name+" as variant "+step.variant+" not found");
	            return new CallRecord();
	
	    }

        JSONObject demographic = new JSONObject();
        demographic.put("uin", "2345");
        demographic.put("name", person.name);
        demographic.put("father_name", person.name);
        demographic.put("dob", "");
        //demographic.put("gender", person.getGender());
        //demographic.put("addressLine1", person.address_line_1);
        //demographic.put("addressLine2", person.address_line_2);
        //demographic.put("addressLine3", person.address_line_3);
        demographic.put("house_no", "34");
        demographic.put("street", "hosur");
        demographic.put("dist", "bangalore");
        demographic.put("state", "karnataka");
        demographic.put("country", "india");
        //demographic.put("region", person.region);
        //demographic.put("province", person.province);
        demographic.put("city", person.city);
        //demographic.put("localAdministrativeAuthority", person.local_administrative_authority);
        
        JSONObject request_json = new JSONObject();
        request_json.put("langCode", "ENG");
        request_json.put("createdBy", person.phone);
        request_json.put("updatedBy", "");
        request_json.put("identity", data_json);
        request_json.put("demographicDetails", demographic);
        request_json.put("createdDateTime", Utils.getCurrentDateAndTimeForAPI());
        request_json.put("updatedDateTime", "");


        JSONObject api_input = new JSONObject();
        api_input.put("id", "mosip.pre-registration.demographic.create");
        api_input.put("ver", "1.0");
        api_input.put("reqTime", Utils.getCurrentDateAndTimeForAPI());
        api_input.put("request", request_json);
        
        String url = "http://localhost:51310/mosip/demographic/update";
        
        
        RestAssured.baseURI = baseUri;
        RestAssured.useRelaxedHTTPSValidation();
        Response api_response = (Response) given().contentType(ContentType.JSON).body(api_input).post(url);
        res = new CallRecord(url, step.name, api_input.toString(), api_response, "" + api_response.getStatusCode());
        AddCallRecord(res);

        /* check for api status */
        if(api_response.getStatusCode() != 200){
            extentTest.log(Status.FAIL, "Assert HTTP STATUS, expected ["+200+"], actual["+api_response.getStatusCode()+"]");
            Utils.auditLog.severe("API HTTP status return as " + api_response.getStatusCode());
            Assert.assertEquals(api_response.getStatusCode(), 200, "API HTTP status");
        }

        /* Error handling middleware */
        if(step.error != null && !step.error.isEmpty()){
            ReadContext ctx = stepErrorMiddleware(step, res);
        }else{
            ReadContext ctx = com.jayway.jsonpath.JsonPath.parse(api_response.getBody().asString());
            /* Response data parsing */
            try {
            	//Long vin= ctx.read("$['vin']");
            	//String VID = vin.toString();
            	//data.persons.get(index).VIDs.add(VID); 
                //System.out.println("kunal checking VIDs :"+ctx.read("$['vin']"));
                
            } catch (PathNotFoundException e){

            }

            /* Assertion policies execution */
            if(step.asserts.size()>0){
                for (assertion_policy assertion_type : step.asserts) {
                    switch (assertion_type) {
                        case DONT:
                            break;
                        /*    
                        case API_CALL:
                            Scenario.Step nstep = new Scenario.Step();
                            nstep.name = "getApplication";
                            nstep.index.add(index);
                            CallRecord getAppRecord = getApplication(nstep);
                            
                            HashMap <String, Object> hm = ResponseParser.addApplicationResponceParser(data.persons.get(index), getAppRecord.response);

                            
                            extentTest.log((Status) hm.get("status"), hm.get("msg").toString());
                            if (hm.get("status").equals(Status.FAIL)) {
                                Utils.auditLog.severe(hm.get("msg").toString());
                            } else {
                                Utils.auditLog.info(hm.get("msg").toString());
                            }
                            Utils.auditLog.info("-----------------------------------------------------------------------------------------------------------------");

                            Assert.assertEquals(hm.get("status"), Status.INFO, hm.get("msg").toString());
                            break;
                        */    
                        case STATUS:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean)expectedStatus, "Assert via STATUS");
                            }
                            break;

                        case DEFAULT:
                            if(!(Boolean) ctx.read("$['status']")){
                                extentTest.log(Status.FAIL, "Assert via STATUS, expected ["+true+"], actual["+ctx.read("$['status']")+"]");
                                Utils.auditLog.severe("API body status return as " + ctx.read("$['status']"));
                                Assert.assertEquals((Boolean) ctx.read("$['status']"), (Boolean) true, "Assert via STATUS");
                            }
                            break;

                        default:
                            extentTest.log(Status.WARNING, "Skipping assert "+assertion_type);
                            Utils.auditLog.warning("Skipping assert "+assertion_type);
                            break;
                    }
                }
            }
        }

		return res;
	}
}
