/*
 * author: Channakeshava
 */

package main.java.io.mosip.ivv.utils;

import main.java.io.mosip.ivv.base.BaseHelper;
import main.java.io.mosip.ivv.base.CallRecord;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Utils {
        public static Properties prop;
        public static String folderPath;
        public static File theDir;
        private static List<String> fileList;
        public static Logger auditLog = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
        public static Logger requestAndresponseLog = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

        /*
         * public static void deleteDirectoryPath(String directoryName, String folderName) { folderPath = directoryName; theDir = new File(folderPath);
         * theDir.delete(); }
         *
         */
        public static void deleteDirectoryPath(String path) {
                File file = new File(path);
                if (file.exists()) {
                        do {
                                deleteIt(file);
                        } while (file.exists());
                } else {
                }
        }

        private static void deleteIt(File file) {
                if (file.isDirectory()) {
                        String fileList[] = file.list();
                        if (fileList.length == 0) {
                                file.delete();
                        } else {
                                int size = fileList.length;
                                for (int i = 0; i < size; i++) {
                                        String fileName = fileList[i];
                                        String fullPath = file.getPath() + "/" + fileName;
                                        File fileOrFolder = new File(fullPath);
                                        deleteIt(fileOrFolder);
                                }
                        }
                } else {
                        file.delete();
                }
        }

        public static String createDirectory(String directoryName, String folderName) {
                folderPath = directoryName + folderName;
                theDir = new File(folderPath);
                if (!theDir.exists()) {
                        try {
                                theDir.mkdirs();
                        } catch (SecurityException se) {
                        }
                }
                return folderPath;
        }

        public static String getCurrentDate() {
                DateFormat format = new SimpleDateFormat("yyyy_MM_dd");
                Date date = new Date();
                return format.format(date).toString();
        }

        public static String getCurrentDateAndTime() {
                DateFormat format = new SimpleDateFormat("yyyy_MM_dd_HH:mm:ss");
                Date date = new Date();
                return format.format(date).toString();
        }

        public static String getCurrentDateAndTimeForAPI() {
                // 2019-01-22T07:22:57.086Z
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                Date date = new Date();
//                ZonedDateTime.now( ZoneOffset.UTC ).format( DateTimeFormatter.ISO_INSTANT )
                return ZonedDateTime.now( ZoneOffset.UTC ).format( DateTimeFormatter.ISO_INSTANT );
        }

        // public void zipTheFolder(String zipFile) {
        public static void zipTheFolder(String SOURCE_FOLDER, String OUTPUT_ZIP_FILE) {
                fileList = new ArrayList<String>();
                byte[] buffer = new byte[1024];
                generateFileList(new File(SOURCE_FOLDER), SOURCE_FOLDER);

                String source = new File(SOURCE_FOLDER).getName();
                FileOutputStream fos = null;
                ZipOutputStream zos = null;
                try {
                        fos = new FileOutputStream(OUTPUT_ZIP_FILE);
                        zos = new ZipOutputStream(fos);
                        FileInputStream in = null;

                        for (String file : fileList) {
                                ZipEntry ze = new ZipEntry(source + File.separator + file);
                                zos.putNextEntry(ze);
                                try {
                                        in = new FileInputStream(SOURCE_FOLDER + File.separator + file);
                                        int len;
                                        while ((len = in.read(buffer)) > 0) {
                                                zos.write(buffer, 0, len);
                                        }
                                } finally {
                                        in.close();
                                }
                        }
                        zos.closeEntry();
                } catch (IOException ex) {
                        ex.printStackTrace();
                } finally {
                        try {
                                zos.close();
                        } catch (IOException e) {
                                e.printStackTrace();
                        }
                }
        }

        public static void generateFileList(File node, String SOURCE_FOLDER) {
                // add file only
                if (node.isFile()) {
                        fileList.add(generateZipEntry(node.toString(), SOURCE_FOLDER));
                }

                if (node.isDirectory()) {
                        String[] subNote = node.list();
                        for (String filename : subNote) {
                                generateFileList(new File(node, filename), SOURCE_FOLDER);
                        }
                }
        }

        private static String generateZipEntry(String file, String SOURCE_FOLDER) {
                return file.substring(SOURCE_FOLDER.length() + 1, file.length());
        }

        public static void setupLogger() {
                LogManager.getLogManager().reset();
                auditLog.setLevel(Level.ALL);

                ConsoleHandler ch = new ConsoleHandler();
                ch.setFormatter(new CustomizedLogFormatter());
                ch.setLevel(Level.ALL);
                auditLog.addHandler(ch);
                try {
                        FileHandler fh = new FileHandler(BaseHelper.auditLogFile, true);
                        fh.setFormatter(new CustomizedLogFormatter());
                        fh.setLevel(Level.ALL);
                        auditLog.addHandler(fh);
                } catch (IOException e) {
                        auditLog.log(Level.SEVERE, "File logger not working", e);
                }
        }

        public static void logRequestAndResponseContent(String requestResponseLogFile) {
                LogManager.getLogManager().reset();
                requestAndresponseLog.setLevel(Level.ALL);
                try {
                        FileHandler fh = new FileHandler(requestResponseLogFile, true);
                        fh.setFormatter(new CustomizedLogFormatter());
                        fh.setLevel(Level.ALL);
                        requestAndresponseLog.addHandler(fh);
                } catch (IOException e) {
                        requestAndresponseLog.log(Level.SEVERE, "File logger not working", e);
                }
        }

        public static void writeRequestAndResponseToFile(String personaDirectory, String fileName, StringWriter requestAndresponseWriter) {
                BufferedWriter bufferedWriter = null;
                try {
                        File myFile = new File(personaDirectory + fileName.replace(":", "_"));
                        if (!myFile.exists()) {
                                myFile.createNewFile();
                        }
                        Writer writer = new FileWriter(myFile);
                        bufferedWriter = new BufferedWriter(writer);
                        bufferedWriter.write(requestAndresponseWriter.toString());
                } catch (IOException e) {
                        e.printStackTrace();
                } finally {
                        try {
                                if (bufferedWriter != null)
                                        bufferedWriter.close();
                        } catch (Exception ex) {

                        }
                }
        }

        public static String assertResponseStatus(String statusCode) {
                String returnString = null;

                switch (Integer.parseInt(statusCode)) {
                        case 200: // Successful or OK
                                returnString = "OK, The request has succeeded";
                                break;
                        case 201: // Created
                                returnString = "Created! The request has been fulfilled and resulted in a new resource being created";
                                break;
                        case 204: // Created
                                returnString = "No Content! The server has fulfilled the request but does not need to return an entity-body, and might want to return updated metainformation";
                                break;
                        case 304: // Created
                                returnString = "Not Modified!";
                                break;
                        case 400: // Bad Request
                                returnString = "Bad Request! The request could not be understood by the server due to malformed syntax";
                                break;
                        case 401: // Unauthorized
                                returnString = "Unauthorized! The request requires user authentication";
                                break;
                        case 403: // Forbidden
                                returnString = "Forbidden! The server understood the request, but is refusing to fulfill it";
                                break;
                        case 404: // Not found
                                returnString = "Not Found! The server has not found anything matching the Request-URI";
                                break;
                        case 405: // Method not allowed
                                returnString = "Method not allowed! The method specified in the Request-Line is not allowed for the resource identified by the Request-URI.";
                                break;
                        case 409: // Conflict
                                returnString = "Conflict! The request could not be completed due to a conflict with the current state of the resource.";
                                break;
                        case 500: // Internal Server Error
                                returnString = "Internal Server Error! The server encountered an unexpected condition which prevented it from fulfilling the request";
                                break;
                        case 503: // Service Unavailable
                                returnString = "Service Unavailable! The server is currently unable to handle the request due to a temporary overloading or maintenance of the server";
                                break;
                }

                return returnString;
        }

}