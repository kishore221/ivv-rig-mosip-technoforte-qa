package main.java.io.mosip.ivv.utils;

import com.aventstack.extentreports.Status;
import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import io.restassured.response.Response;
import main.java.io.mosip.ivv.base.CoreStructures;
import main.java.io.mosip.ivv.base.Persona;
import main.java.io.mosip.ivv.base.ProofDocument;
import main.java.io.mosip.ivv.orchestrator.Scenario;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResponseParser {

    public CoreStructures.Slot slot(String rawData){
        Gson gsn = new Gson();
        Map<String, Object> resMap = gsn.fromJson(rawData, Map.class);
        List center_details = (List)(
                                    (Map)resMap.get("response")
                                ).get("centerDetails");
        if(center_details.size()==0){
            return new CoreStructures.Slot();
        }
        for(Object center_info: center_details) {
            String date = (String)((Map)center_info).get("date");
            List timeSlots = (List)((Map)center_info).get("timeSlots");
            if(timeSlots.size()>0){
                String from = (String)((Map)timeSlots.get(0)).get("fromTime");
                String to = (String)((Map)timeSlots.get(0)).get("toTime");
                return new CoreStructures.Slot(date, from, to);
            }
        }
        return new CoreStructures.Slot();
    }

    public static HashMap<String, Object> addDocumentResponseParser(ProofDocument proofDocument, Response api_response) {
        //TODO - Person, res

        HashMap<String, Object> hm = new HashMap<String, Object>();
        hm.put("status", Status.FAIL);
        hm.put("msg", "No Documents uploaded");

        ReadContext ctx = JsonPath.parse(api_response.getBody().asString());
        ArrayList<HashMap<String, String>> documents = ctx.read("$['response']");
        for (int i=0; i < documents.size(); i++) {
            if (proofDocument.doc_id.equals(documents.get(i).get("doc_id"))) {
                hm.put("status", Status.INFO);
                hm.put("msg", "Document '" + proofDocument.doc_cat_code.toString().trim() + "' uploaded");

                if(proofDocument.name.compareTo(documents.get(i).get("doc_name")) != 0){
                    hm.put("status", Status.FAIL);
                    hm.put("msg", "Expected: " + proofDocument.name + ", Actual: " + documents.get(i).get("doc_name") + ", document response matching");
                    return hm;
                }
                if(proofDocument.doc_cat_code.compareTo(ProofDocument.DOCUMENT_CATEGORY.valueOf(documents.get(i).get("doc_cat_code"))) != 0){
                    hm.put("status", Status.FAIL);
                    hm.put("msg", "Expected: " + proofDocument.doc_cat_code + ", Actual: " + documents.get(i).get("doc_cat_code") + ", document response matching");
                    return hm;
                }
                if(proofDocument.doc_type_code.compareTo(documents.get(i).get("doc_typ_code")) != 0){
                    hm.put("status", Status.FAIL);
                    hm.put("msg", "Expected: " + proofDocument.doc_type_code + ", Actual: " + documents.get(i).get("doc_typ_code") + ", document response matching");
                    return hm;
                }
                if(proofDocument.doc_file_format.compareTo(documents.get(i).get("doc_file_format")) != 0){
                    hm.put("status", Status.FAIL);
                    hm.put("msg", "Expected: " + proofDocument.doc_file_format + ", Actual: " + documents.get(i).get("doc_file_format") + ", document response matching");
                    return hm;
                }
            }
        }

        return hm;
    }
    
    public static HashMap<String, Object> addApplicationResponceParser(Persona person, Response api_response) {
        //TODO - Person, res

        HashMap<String, Object> hm = new HashMap<String, Object>();
        hm.put("status", Status.FAIL);
        hm.put("msg", "No Application Create");

        ReadContext ctx = JsonPath.parse(api_response.getBody().asString());
        HashMap<String, String> app_info = ctx.read("$['response'][0]");
        
            if (!person.phone.equals(app_info.get("phone"))) {
                hm.put("status", Status.INFO);
                hm.put("msg", "Application Created");

            }
            if (!person.cnie_number.equals(app_info.get("CNIENumber"))) {
                hm.put("status", Status.INFO);
                hm.put("msg", "Expected: " + person.cnie_number + ", Actual: " + app_info.get("CNIENumber") + " applicatoin response matching");
                return hm;
            }
            if (!person.email.equals(app_info.get("email"))) {
                hm.put("status", Status.INFO);
                hm.put("msg", "Expected: " + person.email + ", Actual: " + app_info.get("email") + " applicatoin response matching");
            }
            if (!person.postal_code.equals(app_info.get("postalCode"))) {
                hm.put("status", Status.INFO);
                hm.put("msg", "Expected: " + person.postal_code + ", Actual: " + app_info.get("postalCode") + " applicatoin response matching");
            }

        return hm;
    }
    
    public static HashMap<String, Object> updateApplicationResponceParser(Persona person, Response api_response) {
        //TODO - Person, res

        HashMap<String, Object> hm = new HashMap<String, Object>();
        hm.put("status", Status.FAIL);
        hm.put("msg", "No Application Updated");

        ReadContext ctx = JsonPath.parse(api_response.getBody().asString());
        HashMap<String, String> udpate_info = ctx.read("$['response'][0]");
            if (!person.phone.equals(udpate_info.get("phone"))) {
                hm.put("status", Status.INFO);
                return hm;
            }
            if (!person.date_of_birth.equals(udpate_info.get("dateOfBirth"))) {
                hm.put("status", Status.INFO);
                hm.put("msg", "Expected: " + person.date_of_birth + ", Actual: " + udpate_info.get("dateOfBirth") + " applicatoin response matching");
            }
            if (!person.name.equals(udpate_info.get("name"))) {
                hm.put("status", Status.INFO);
                hm.put("msg", "Expected: " + person.name + ", Actual: " + udpate_info.get("fullName") + " applicatoin response matching");
            }
            if (!person.email.equals(udpate_info.get("email"))) {
                hm.put("status", Status.INFO);
                hm.put("msg", "Expected: " + person.email + ", Actual: " + udpate_info.get("email") + " applicatoin response matching");
            }
            if (!person.postal_code.equals(udpate_info.get("postalCode"))) {
                hm.put("status", Status.INFO);
                hm.put("msg", "Expected: " + person.postal_code + ", Actual: " + udpate_info.get("postalCode") + " applicatoin response matching");
            }

        return hm;
    }

    
    
    public static HashMap<String, Object> bookAppointmentResponseParser(Persona person, Response api_response){
        HashMap<String, Object> hm = new HashMap<String, Object>();
        hm.put("status", Status.INFO);
        hm.put("msg", "book appointment asserted successfully");

        ReadContext ctx = JsonPath.parse(api_response.getBody().asString());
        HashMap<String, String> appointment_info = ctx.read("$['response']");
        if(!person.registration_center_id.equals(appointment_info.get("registration_center_id"))){
            hm.put("status", Status.FAIL);
            hm.put("msg", "Expected: "+person.registration_center_id+", Actual: "+appointment_info.get("registration_center_id") + ", registration center id");
            return hm;
        }
        if(!person.slot.date.equals(appointment_info.get("appointment_date"))){
            hm.put("status", Status.FAIL);
            hm.put("msg", "Expected: "+person.slot.date+", Actual: "+appointment_info.get("appointment_date") + ", appointment date");
            return hm;
        }
        if(!person.slot.from.equals(appointment_info.get("time_slot_from"))){
            hm.put("status", Status.FAIL);
            hm.put("msg", "Expected: "+person.slot.from+", Actual: "+appointment_info.get("time_slot_from") + ", appointment time from");
            return hm;
        }
        if(!person.slot.to.equals(appointment_info.get("time_slot_to"))){
            hm.put("status", Status.FAIL);
            hm.put("msg", "Expected: "+person.slot.to+", Actual: "+appointment_info.get("time_slot_to") + ", appointment time to");
            return hm;
        }
        return hm;
    }

}
